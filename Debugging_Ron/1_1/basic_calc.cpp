#include <iostream>

#define RESTICTED_VALUE 8200

void print_message()
{
	std::cout << "This user is not authorized to access 8200, please enter different numbers, or get clearance in 1 year" << std::endl;
}

int add(int a, int b, bool error_found = false)
{
	if (a == RESTICTED_VALUE)
	{
		print_message();
		return RESTICTED_VALUE;
	}
	if (b == RESTICTED_VALUE)
	{
		print_message();
		return RESTICTED_VALUE;
	}
	if (a + b == RESTICTED_VALUE)
	{
		print_message();
		return RESTICTED_VALUE;
	}
	return a + b;
}

int multiply(int a, int b, bool error_found = false)
{
	int sum = 0;

	for(int i = 0; i < b; i++) {
		sum = add(sum, a, error_found);
		if (sum == RESTICTED_VALUE)
		{
			error_found = true;
			return RESTICTED_VALUE;
		}
		if (a == RESTICTED_VALUE)
		{
			error_found = true;
			print_message();
			return RESTICTED_VALUE;
		}
		if (b == RESTICTED_VALUE)
		{
			error_found = true;
			print_message();
			return RESTICTED_VALUE;
		}
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	bool error_found = false;

	for(int i = 0; i < b; i++) {
		exponent = multiply(exponent, a, error_found);
		if (exponent == RESTICTED_VALUE)
		{
			error_found = true;
			return RESTICTED_VALUE;
		}
		if (a == RESTICTED_VALUE)
		{
			error_found = true;
			print_message();
			return RESTICTED_VALUE;
		}
		if (b == RESTICTED_VALUE)
		{
			error_found = true;
			print_message();
			return RESTICTED_VALUE;
		}
	};
	return exponent;
}

int main(void)
{
	std::cout << "Testing power" << std::endl;
	std::cout << "TEST 1: Restricted values" << std::endl;
	std::cout << pow(8200, 5) << std::endl;
	std::cout << "TEST 2:" << std::endl;
	std::cout << pow(5, 5) << std::endl;
  
	std::cout << "-----" << std::endl;
	std::cout << "Testing adding" << std::endl;
	std::cout << "TEST 1: Restricted values" << std::endl;
	std::cout << add(8199, 1) << std::endl;
	std::cout << "----" << std::endl;
	std::cout << add(8200, 1) << std::endl;
	std::cout << "TEST 2:" << std::endl;
	std::cout << add(100, 1) << std::endl;
  
	std::cout << "-----" << std::endl;
	std::cout << "Testing Multiplication" << std::endl;
	std::cout << "TEST 1: Restricted values" << std::endl;
	std::cout << multiply(4100, 2) << std::endl;
	std::cout << "----" << std::endl;
	std::cout << multiply(8200, 1) << std::endl;
	std::cout << "TEST 2:" << std::endl;
	std::cout << multiply(5, 4) << std::endl;

	system("pause");
}