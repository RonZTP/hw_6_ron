#include <iostream>
#include <string>

#define RESTIRCTED_VALUE 8200
#define ERROR_MESSAGE "This user is not authorized to access 8200, please enter different numbers, or get clearance in 1 year"

int add(int a, int b)
{
	if (a + b == RESTIRCTED_VALUE || a == RESTIRCTED_VALUE || b == RESTIRCTED_VALUE)
	{
		throw(std::string(ERROR_MESSAGE));
	}
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;

	for(int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == RESTIRCTED_VALUE || a == RESTIRCTED_VALUE || b == RESTIRCTED_VALUE)
		{
			throw(std::string(ERROR_MESSAGE));
		}
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;

	for(int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == RESTIRCTED_VALUE || a == RESTIRCTED_VALUE || b == RESTIRCTED_VALUE)
		{
			throw(std::string(ERROR_MESSAGE));
		}
	};
	return exponent;
}

int main(void)
{
	std::cout << "Testing power" << std::endl;
	std::cout << "TEST 1: Restricted values" << std::endl;
	try
	{
		std::cout << pow(8200, 5) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}
	std::cout << "TEST 2:" << std::endl;
	try
	{
		std::cout << pow(5, 5) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}
	

	std::cout << "-----" << std::endl;
	std::cout << "Testing adding" << std::endl;
	std::cout << "TEST 1: Restricted values" << std::endl;
	try
	{
		std::cout << add(8199, 1) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}
	std::cout << "----" << std::endl;
	try
	{
		std::cout << add(8200, 1) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}


	std::cout << "TEST 2:" << std::endl;
	try
	{
		std::cout << add(100, 1) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}
	

	std::cout << "-----" << std::endl;
	std::cout << "Testing Multiplication" << std::endl;
	std::cout << "TEST 1: Restricted values" << std::endl;
	try
	{
		std::cout << multiply(4100, 2) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}
	std::cout << "----" << std::endl;
	try
	{
		std::cout << multiply(8200, 1) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}


	std::cout << "TEST 2:" << std::endl;
	try
	{
		std::cout << multiply(5, 4) << std::endl;
	}
	catch (const std::string& error_message)
	{
		std::cout << error_message << std::endl;
	}
	system("pause");
}